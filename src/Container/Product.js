import { connect } from 'react-redux';
import Product from '../Product/Product';
import { add, update, remove, set, addPlus, updatePlus } from '../store/product';

const mapStateToProps = (state) => {
    return {
        products: state.product.items
    }
}

const mapStateToAction = (dispatch) => ({
    add: (text) => dispatch(add(text)),

    update: (text, index) => dispatch(update(text, index)),

    remove: (index) => dispatch(remove(index)),

    set: (item) => dispatch(set(item)),

    addPlus: (text) => dispatch(addPlus(text)),

    updatePlus: (text, index) => dispatch(updatePlus(text, index))
}) 


export default connect(mapStateToProps, mapStateToAction)(Product);