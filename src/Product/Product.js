import React, { useEffect, useState } from "react";

import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

import GetMealFromApi from "./GetMealFromApi";

function Product({ products, remove, addPlus, updatePlus }) {

  useEffect(() => {

  }, []);


  const [name, setName] = useState("");
  const [error, setError] = useState("");
  const [show, setShow] = useState(false);

  const [modalTitle, setModalTitle] = useState('');
  const [isDelete, setIsDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [index, setIndex] = useState(-1);
  const [curentName, setCurentName] = useState('');

  const handleClose = () => {
    setShow(false);
    setIsEdit(false);
    setIsDelete(false);
    setCurentName('');
    setIndex(-1);
    setError("");
  };

  const addNew = () => {
    setIsEdit(false);
    setName("");
    setModalTitle("Add New");
    setShow(true);
  };

  const handleEdit = (item) => {
    setIsEdit(true);
    setModalTitle("Edit meal");
    let index = products.indexOf(item)
    setIndex(index)
    setShow(true);
    setName(item.name);
  };

  const handleEditModal = () => {
    updatePlus(name, index);
    setShow(false);
  };

  const handleDelete = (item) => {
    setModalTitle("Delete");
    let index = products.indexOf(item)
    setIndex(index)
    setIsDelete(true);
    setShow(true);
    setCurentName(item.name)
  }

  const handleDeleteModal = () => {
    remove(index);
    setShow(false);
    setIsDelete(false);
  }

  const addNewSubmit = (e) => {
    e.preventDefault();
    if (
      name === "" ||
      name.trim() === "" ||
      name === null ||
      name === undefined
    ) {
      return setError("Name is required");
    }
    addPlus(name);
    setShow(false);
    setError("");

  };

  return (
    <div className="Meal">
      <Container>
        <h1>Poeta Front-end Developer Testing</h1>
        <h3>Get all meals with the name “Arrabiata” from the API:</h3>
        <GetMealFromApi />

        <hr className="mb-5" />

        <h3>
          {" "}
          Make CRUD functions manage matches the UI design (no need paging)
        </h3>
        <Button
          className="mb-2 float-end"
          variant="success"
          onClick={addNew}
        >
          Add New
        </Button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {!isDelete ? (<Form onSubmit={addNewSubmit}>
              <Form.Group className="mb-3" controlId="formBasicName">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Name"
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                />
                <p className="mt-0 error">{error}</p>
              </Form.Group>

              <div className="mt-3 clearfix">

                {isEdit ? (<Button
                  className="float-end"
                  onClick={handleEditModal}
                  variant="primary"
                >
                  Edit
                </Button>) : (<Button className="float-end" type="submit" variant="primary">
                  Add
                </Button>)}

              </div>
            </Form>) :
              (
                <div>
                  <p> Are you want to delete {curentName} ? </p>

                  <Button
                    className="float-end"
                    onClick={handleDeleteModal}
                    variant="primary"
                  >
                    Delete
                  </Button>
                </div>
              )
            }
          </Modal.Body>
        </Modal>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Meal</th>
              <th>Count</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {products.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.count}</td>
                <td>
                  <Button variant="primary" onClick={() => handleEdit(item)}>Edit </Button>
                  <Button className="mx-2" variant="primary" onClick={() => handleDelete(item)}>Delete </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}

export default Product;
