import React, { useEffect, useState } from "react";

import Table from "react-bootstrap/Table";
import axios from "axios";

function GetMealFromApi() {
  const [mealsSearch, setMealsSeach] = useState([]);

  useEffect(() => {
    axios.get("https://www.themealdb.com/api/json/v1/1/search.php?s=Arrabiata").then((res) => {
      setMealsSeach(res.data.meals);
    });
  }, []);

  return (
    <div className="Meal">
      <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Meal</th>
            </tr>
          </thead>
          <tbody>
            {mealsSearch.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.strMeal}</td>
              </tr>
            ))}
          </tbody>
        </Table>
    </div>
  );
}

export default GetMealFromApi;
