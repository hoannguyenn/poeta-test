
import { combineReducers } from 'redux';

import { productReducer } from './product';
import { configureStore } from '@reduxjs/toolkit';

const reducer = combineReducers({
    product: productReducer,
})


const assynMiddleware = store => next => action => {
    if (typeof action === 'function') {
        action(next);
    } else {
        return next(action);
    }
}


// export default createStore(
//     reducer,
//     compose(
//         applyMiddleware(
//             assynMiddleware,
//             thunk
//         ),

//     )

// );

// export default configureStore({
//     reducer
// })

export default configureStore({
    reducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(assynMiddleware),
    devTools: process.env.NODE_ENV !== 'production',
})