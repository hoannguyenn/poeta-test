import axios from "axios";

const initState = {
  items: [
    {
      name: "Arrabiata",
      count: 1,
    },
  ],
};

const ADD = "ADD";
const REMOVE = "REMOVE";
const SET = "SET";
const UPDATE = "UPDATE";

export const add = (text, count) => {
  return {
    type: ADD,
    payload: {
      name: text,
      count: count,
    },
  };
};

export const addPlus = (text) => async (dispatch) => {
  const res = await axios.get(
    `https://www.themealdb.com/api/json/v1/1/search.php?s=${text}`
  );
  
  if(res.data.meals !== null) {
    let count = res.data.meals.length;
    dispatch(add(text, count));
  } else {
    dispatch(add(text, 0));
    console.log('meals:' + res.data.meals);
  }
};


export const update = (text, index, count) => {
  return {
    type: UPDATE,
    payload: {
      name: text,
      index: index,
      count: count
    },
  };
};

export const updatePlus = (text, index) => async (dispatch) => {
  const res = await axios.get(
    `https://www.themealdb.com/api/json/v1/1/search.php?s=${text}`
  );
  
  if(res.data.meals !== null) {
    let count = res.data.meals.length;
    dispatch(update(text, index, count));
  } else {
    dispatch(update(text, index, 0));
    console.log('meals:' + res.data.meals);
  }
};

export const remove = (index) => {
  return {
    type: REMOVE,
    payload: {
      index: index,
    },
  };
};

export const set = (item) => {
  return {
    type: SET,
    payload: item,
  };
};

export const productReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD:
      return {
        ...state,
        items: [
          {
            name: action.payload.name,
            count: action.payload.count,
          },
          ...state.items,
        ],
      };

    case UPDATE:
      return {
        ...state,
        items: [
          ...state.items.slice(0, action.payload.index),
          {
            ...state.items[action.payload.index],
            name: action.payload.name,
            count: action.payload.count
          },
          ...state.items.slice(action.payload.index + 1),
        ],
      };

    case REMOVE:
      return {
        ...state,

        items: [
          ...state.items.filter(
            (item) => state.items.indexOf(item) !== action.payload.index
          ),
        ],
      };

    case SET:
      return {
        ...state,
        items: action.payload,
      };

    default:
      return state;
  }
};
