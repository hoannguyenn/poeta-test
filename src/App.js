
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Provider } from 'react-redux';
import store from './store/store';
import Product from './Container/Product';

function App() {
  return (
    <div className="App">
      
      <Provider store={store}>
        <Product/>
      </Provider>
    </div>
  );
}

export default App;
